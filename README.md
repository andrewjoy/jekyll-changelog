jekyll-changelog
===

Generate automatic changelogs via git for any file on a Jekyll site.

Installation
---

1. Get the gem.

        gem install jekyll-changelog

2. If you're on Windows, make sure you have a working `diff` on your machine.

    Install [msys2](http://www.msys2.org/).
    
    OR
    
    Follow installation instructions in the [diffy](https://github.com/samg/diffy) dependency.

Usage
---

Params:

    changelog(path : string, fromDate : Date, toDate : Date = Date.today) : string
    
Jekyll:

    {{ page.path | changelog: page.releaseDate }}

This is a slow process and will slow down your builds, so any calls to changelog should be restricted to a production environment.