lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'version.rb'

Gem::Specification.new do |spec|
    spec.name = 'jekyll-changelog'
    spec.version = Jekyll::Changelog::VERSION;
    spec.date = '2019-04-18'
    spec.summary = 'Jekyll Changelog plugin'
    spec.description = 'Git-powered changelogs for Jekyll files.'
    spec.authors = ['Andrew Joy']
    spec.email = 'therealandrewjoy@gmail.com'
    spec.files = *Dir["lib/**/*.rb"]
    spec.homepage = 'https://gitlab.com/andrewjoy/jekyll-changelog'
    spec.license = 'MIT'

    spec.add_dependency 'diffy', '~> 3.0'
    
    spec.add_development_dependency 'jekyll', '~> 3.0'
end