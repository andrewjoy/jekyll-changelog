require 'diffy'

module Jekyll
    module Changelog
        def self.get_commit(path, date)
            `git log -1 --before=@{#{date}T00:00:00} --format=%H -- #{path}`.strip
        end

        def self.get_content(path, commit)
            `git show "#{commit}:#{path}"`
        end

        module Filter
            def changelog(path, fromDate, toDate = Date.today)
                fromCommit = Jekyll::Changelog.get_commit(path, fromDate + 1)
                toCommit = Jekyll::Changelog.get_commit(path, toDate + 1)

                fromContent = Jekyll::Changelog.get_content(path, fromCommit)
                toContent = Jekyll::Changelog.get_content(path, toCommit)

                Diffy::Diff.new(fromContent, toContent, :context => 0).to_s(:html)
            end
        end
    end
end

Liquid::Template.register_filter(Jekyll::Changelog::Filter)
